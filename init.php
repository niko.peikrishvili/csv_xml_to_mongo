<?php

require_once __DIR__ . '/vendor/autoload.php';
$xmltomongo = new XmlToMongo\XmlToMongo();
try{
    

$mongo = new \XmlToMongo\MongoDatabase($xmltomongo->settings->mongoconnectionstring,$xmltomongo->settings->mongocollection);

$files = $xmltomongo->getFilesList();
$tables = [];
foreach ($files as $file) {
    $parsed = $xmltomongo->parseCSVFromFile($file);
    $file = str_replace('.csv', '', $file);

    foreach ($parsed as $url) {
        try {
            $xmlObject = $xmltomongo->getXmlObjectFromUrl(trim($url));
            foreach($xmlObject as $single)
            {
                $data = $xmltomongo->getArrayFromXmlObject($single);
                $data['source'] = trim($url);
                $mongo->insertTodb($file, $data);
            }
        } catch (\Throwable $ex) {
            echo $ex->getMessage()."\n";
        }


    }
}
} catch (\Exception $ex) {
    echo "Error : somethong went wrong : ".$ex->getMessage()."\n";
}