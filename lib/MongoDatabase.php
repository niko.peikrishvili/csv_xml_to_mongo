<?php

namespace XmlToMongo;

/**
 * Description of MongoDatabase
 *
 * @author niko.peikrishvili
 */
class MongoDatabase {

    protected $client;
    protected $collection;

    public function __construct($connection, $collection) {
        try {
            echo $connection;
            $this->client = new \MongoDB\Client($connection);
            $this->collection = $collection;
        } catch (\Exception $ex) {
            throw new Exception("Error while connecting to mongo database, reason : " . $ex->getMessage());
        }
    }

    public function insertTodb($tableName, $data) {
        $this->client->{$this->collection}->{$tableName}->insertOne($data);
    }

}
