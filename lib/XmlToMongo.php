<?php

namespace XmlToMongo;

use \Exception;
use \GuzzleHttp\Exception\ClientException;

/**
 * Description of XmlToMongo
 *
 * @author niko.peikrishvili
 */
class XmlToMongo {

    protected $settingFile = '/settings.json';
    public $settings;
    public function __construct() {
        
        $this->settings = json_decode(file_get_contents(__DIR__ . '/..' . $this->settingFile));
    }

    public function getFilesList() {
        $dir = scandir(__DIR__ . '/..' . $this->settings->csvfilelocations);
        return array_diff($dir, array('..', '.'));
    }

    public function parseCSVFromFile($file) {
        return file(__dir__ . '/..' . $this->settings->csvfilelocations . $file);
    }

    public function getXmlObjectFromUrl($url) {

        $client = new \GuzzleHttp\Client();
        try {

            if (substr($url, 0, 3) == 'www') {
                $url = 'http://' . $url;
            }
            $response = $client->request('GET', $url);
        } catch (ClientException $ex) {
            throw new Exception("Error while connecting to " . $url . " endpoint");
        }
        if ($response->getStatusCode() != 200) {
            throw new Exception("Error while connecting to " . $url . " endpoint", $response->getStatusCode());
        }
        $responseObject = simplexml_load_string($this->cleanTags($response->getBody()->getContents()));
        if (!$responseObject) {
            throw new Exception("Cannot parse XML from response : $url");
        }

        return $responseObject;
    }

    public function getArrayFromXmlObject($single) {
        $item = [];
        $item['url'] = (string)$single->loc;
        $item['image'] = '';
        $item['title'] = '';
        if (property_exists($single, 'image')) {
            if (property_exists($single->image, 'url')) {
                $item['image'] = (string)$single->image->url;
            }
            if (property_exists($single->image, 'title')) {
                $item['title'] = html_entity_decode((string)$single->image->title,ENT_QUOTES | ENT_XML1, 'UTF-8');
            }
        }
        return $item;
    }

    public function cleanTags($content) {
        $content = str_replace("image:image", "image", $content);
        $content = str_replace("image:loc", "url", $content);
        $content = str_replace("image:caption", "title", $content);
        return $content;
    }

    public function getTestData()
    {
        $url = "http://www.foodfacts.com/ci/nutritionfacts/vitamins-nutritional-dietary/bsn-vanilla-true-mass-gainer-575-lbs/95182";
    
        $client = new \GuzzleHttp\Client();
        try {

            
            $response = $client->request('GET', $url,['headers'=>[
                'User-Agent'=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',
                'data-sitekey'=>'6LeCLhIUAAAAAJdwm5fXxWx1wvbl1VU4C0m4d0GM']]);
            return $response->getBody()->getContents();
        } catch (ClientException $ex) {
            throw new Exception("Error while connecting to " . $url . " endpoint");
        }
        if ($response->getStatusCode() != 200) {
            throw new Exception("Error while connecting to " . $url . " endpoint", $response->getStatusCode());
        }
        $responseObject = simplexml_load_string($this->cleanTags($response->getBody()->getContents()));
        if (!$responseObject) {
            throw new Exception("Cannot parse XML from response : $url");
        }

        return $responseObject;
        
    }
}
